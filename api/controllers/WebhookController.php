<?php

namespace api\controllers;

use api\services\GitService;
use api\services\JiraService;
use api\services\TelegramBotService;
use common\models\TgJiraUser;
use yii\rest\Controller;

class WebhookController extends Controller
{
    public function actionIndex() {
        $data = \Yii::$app->request->post();

        $service = new GitService($data);
        $service->handle();

        return \Yii::$app->response->setStatusCode(200);
    }

    public function actionTelegramBot() {
        // Сделал какой то таск
        $data = \Yii::$app->request->post();
        $botService = new TelegramBotService($data);
        if (isset($data['callback_query'])) {
            $botService->handleCallback();
        } else {
            $botService->handleMessage();
        }
    }

    public function actionJira() {
        $data = \Yii::$app->request->post();
        $jira = new JiraService($data);
        $jira->handle();
        return \Yii::$app->response->setStatusCode(200);
    }

    public function actionError() {
        var_dump(\Yii::$app->errorHandler->exception);
        return \Yii::$app->response->setStatusCode(200);
    }
}