<?php


namespace api\services;

use aki\telegram\base\Command;
use aki\telegram\Telegram;
use common\models\JiraProjects;
use common\models\TgJiraUser;
use common\models\User;

class TelegramBotService
{
    public $data;
    public $chatId;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handleMessage() {
        $message = $this->data['message']['text'];
        $this->chatId = $this->data['message']['chat']['id'];

        if (strpos($message, '◀️Вернуться в меню') !== false) {
            $this->createMenu(\Yii::$app->telegram);
            die();
        }

        Command::run('/start', function ($telegram) {
            $this->createMenu($telegram);
        });

        if (strpos($message, 'Создать таск') !== false) {
            $this->clearSession();
            $this->setSession(0, 'command', 'create_task');
            $this->handleCreateTask();
        } elseif (strpos($message, 'Получить статусы задач') !== false) {
            $this->clearSession();
            $this->setSession(0, 'command', 'get_statuses');
            $this->executeGetStatuses();
        } elseif (strpos($message, 'Добавить комментарий 🧐') !== false) {
            $this->clearSession();
            $this->setSession(1, 'command', 'add_comment');
            $this->sendMessage('Введите ключ задачи');
        } elseif (strpos($message, 'Изминить статус моей задачи 📋') !== false) {
            $this->clearSession();
            $this->setSession(1, 'command', 'change_status');
            $this->sendMessage('Введите ключ задачи');
        } elseif (strpos($message, 'Назначить задачу ✏') !== false) {
            $this->clearSession();
            $this->setSession(1, 'command', 'assign_task');
            $this->sendMessage('Введите ключ задачи');
        } else {
            $session = $this->getSession();
            if ($session && isset($session['step']) && $session['step'] > 0) {
                if ($session['command'] == 'create_task') {
                    $this->executeCreateTask($message);
                } elseif ($session['command'] == 'get_statuses') {
                    $this->executeGetStatuses();
                } elseif ($session['command'] == 'add_comment') {
                    $this->executeAddComment($message);
                } elseif ($session['command'] == 'change_status') {
                    $this->executeChangeStatus($message);
                } elseif ($session['command'] == 'assign_task') {
                    $this->executeAssignTask($message);
                }
            }
        }
    }

    public function createMenu($telegram) {
        $this->clearSession();
        $this->addTelegramId($telegram);

        $telegram->sendMessage([
            'chat_id' => $telegram->input->message->chat->id,
            'text' => 'Выберите комманду',
            'reply_markup' => json_encode([
                'keyboard' => [
                    [
                        ['text' => 'Создать таск 🖥'],
                    ],
                    [
                        ['text' => 'Добавить комментарий 🧐']
                    ],
                    [
                        ['text' => 'Изминить статус моей задачи 📋']
                    ],
                    [
                        ['text' => 'Получить статусы задач 📑']
                    ],
                    [
                        ['text' => 'Назначить задачу ✏']
                    ]
                ],
                'resize_keyboard' => true
            ])
        ]);
    }

    public function executeAssignTask($message) {
        $session = $this->getSession();
        if ($session['step'] == 1) {
            $this->setSession(2, 'task_key', $message);
            $users = TgJiraUser::find()
                ->joinWith('userProjects')
                ->joinWith('userProjects.project')
                ->where(['is not', 'jira_id', null])
                ->andWhere(['is not', 'jira_username', null])
                ->andWhere(['=', 'key', explode('-', $message)[0]])
                ->all();
            $temp = [];
            $keyboard = [];

            foreach ($users as $key => $user) {
                $temp[] = ['text' => $user->jira_username];
                if (($key + 1) % 3 == 0) {
                    $keyboard[] = $temp;
                    $temp = [];
                } elseif ((sizeof($users) - 2) < $key) {
                    $keyboard[] = $temp;
                }
            }

            $keyboard[] = [['text' => '◀️Вернуться в меню']];

            if (!$users) {
                $this->sendMessage('<i>В проекте нет участников</i>');
            } else {
                \Yii::$app->telegram->sendMessage([
                    'chat_id' => $this->chatId,
                    'text' => '<b>Выберите пользователя</b>',
                    'parse_mode' => 'html',
                    'disable_web_page_preview' => true,
                    'reply_markup' => json_encode([
                        'keyboard' => $keyboard,
                        'resize_keyboard' => true
                    ])
                ]);
            }
        } elseif ($session['step'] == 2) {
            $jira = new JiraService($this->data);
            //asd
            $jira_user = TgJiraUser::findOne(['jira_username' => $message]);
            $jira->setAssignee($jira_user->jira_id, $session['task_key']);
            $this->sendMessage('<i>Исполнитель назначен</i>');
        }
    }

    public function executeChangeStatus($message) {
        $session = $this->getSession();
        if ($session['step'] == 1) {
            $this->setSession(2, 'task_key', $message);
            $jira = new JiraService($this->data);
            $statuses = $jira->getAvailableStatuses($message);

            $statusesKeyboard = [];

            foreach ($statuses as $status) {
                $statusesKeyboard[] = ['text' => $status['to']['name']];
            }

            // test

            \Yii::$app->telegram->sendMessage([
                'chat_id' => $this->chatId,
                'text' => '<b>Выберите статус</b>',
                'parse_mode' => 'html',
                'disable_web_page_preview' => true,
                'reply_markup' => json_encode([
                    'keyboard' => [
                        $statusesKeyboard,
                        [
                            ['text' => '◀️Вернуться в меню']
                        ]
                    ],
                    'resize_keyboard' => true
                ])
            ]);
        } elseif ($session['step'] == 2) {
            $this->setSession(3, 'status', $message);

            $jira = new JiraService($this->data);
            $statusId = 0;
            $statuses = $jira->getAvailableStatuses($session['task_key']);
            foreach ($statuses as $status) {

                if ($status['to']['name'] == $message) {
                    $statusId = $status['id'];
                }
            }

            $jira->setTaskStatus($statusId, $session['task_key']);
            $this->createMenu(\Yii::$app->telegram);
        }
    }

    public function executeAddComment($message) {
        $session = $this->getSession();
        if ($session['step'] == 1) {
            $this->setSession(2, 'task_key', $message);
            $this->sendMessage('Введите комментарий');
        } elseif ($session['step'] == 2) {
            $this->setSession(3, 'comment', $message);
            $this->addComment();
        }
    }

    public function addComment() {
        $session = $this->getSession();
        $curl = curl_init();

        $comment = $session['comment'] . " \\n _Отправитель_: *{$this->data['message']['from']['last_name']} {$this->data['message']['from']['first_name']}* | _Телеграм_: [@{$this->data['message']['from']['username']}|https://web.telegram.org/#/im?p=%40{$this->data['message']['from']['username']}]";

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://'.JiraService::$jira_domain.'.atlassian.net/rest/api/2/issue/'.$session['task_key'].'/comment',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "body": "'.$comment.'"
            }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . JiraService::$jira_token,
                'Content-Type: application/json',
                'Cookie: atlassian.xsrf.token=b23cfa38-d552-4b6e-9261-e8225bdf60c2_964a06afbc3a98187d0ed119611ebca7f2173292_lin'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $this->sendMessage('<i>Комментарий оставлен</i>');
    }

    public function executeGetStatuses() {
        $curl = curl_init();

        $username = $this->data['message']['from']['username'];
        $user = TgJiraUser::findOne(['telegram_username' => $username]);

        $jira_username = urlencode($user->jira_username);
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://'.JiraService::$jira_domain.'.atlassian.net/rest/api/3/search?fields=summary,status&jql=status!=%22done%22%20AND%20(creator=%22'.$jira_username.'%22'.'%20OR%20reporter=%22'.$jira_username.'%22)',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . JiraService::$jira_token,
                'Cookie: atlassian.xsrf.token=7ed0d05f-a7f7-4cb4-b6fc-5a231a73ec20_c7736b874cc29a6a4e1905fc292a22460b5b8ce5_lin'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($response, JSON_FORCE_OBJECT);

        $responseMessage = '';
        foreach ($response['issues'] as $task) {
            $responseMessage .= '<a href="https://'.JiraService::$jira_domain.'.atlassian.net/browse/'.$task['key'].'"><b>' . $task['key'] . '</b></a>: ' . $task['fields']['summary'] . ' | ' . $task['fields']['status']['name'] . "\n";
        }
        if (!$responseMessage) {
            $responseMessage = '<b>Задач нет</b>';
        }
        \Yii::$app->telegram->sendMessage([
            'text' => $responseMessage,
            'chat_id' => $this->chatId,
            'parse_mode' => 'html',
            'disable_web_page_preview' => true
        ]);
    }

    public function executeCreateTask($message) {
        $session = $this->getSession();
        if ($session['step'] == 1) {
            $this->setSession(2, 'title', $message);
            $this->sendMessage('<i>Тема установлена</i>');
            $this->sendMessage('<b>Введите описание задачи</b>');
        }  else if ($session['step'] == 2) {
            $this->setSession(3, 'detail', $message);
            $this->sendMessage('<i>Описание установлено</i>');
            \Yii::$app->telegram->sendMessage([
                'chat_id' => $this->chatId,
                'text' => '<b>Выберите приоритет</b>',
                'parse_mode' => 'html',
                'reply_markup' => json_encode([
                    'keyboard' => [
                        [
                            ['text' => 'Очень Важно'],
                            ['text' => 'Важно'],
                            ['text' => 'Средне'],
                            ['text' => 'Не Важно']
                        ],
                        [
                            ['text' => '◀️Вернуться в меню']
                        ]
                    ],
                    'resize_keyboard' => true
                ])
            ]);
        } else if ($session['step'] == 3) {
            $this->setSession(4, 'priority', $message);
            $this->sendMessage('<i>Приоритет установлен</i>');
            $this->sendMessage('<b>Прикрепите файл или напишите "нет"</b>');
        } else if ($session['step'] == 4) {
            $session = $this->getSession();
            $projectId = explode('-', $session['project'])[1];

            $project = JiraProjects::findOne(['id' => $projectId]);
            $tgJiraUser = TgJiraUser::findOne(['telegram_username' => $this->data['message']['from']['username']]);

            if ($tgJiraUser && $tgJiraUser->jira_id) {
                $authorId = $tgJiraUser->jira_id;
            } else {
                $authorId = false;
            }


            if (isset($this->data['message']['document'])) {
                $fileId = $this->data['message']['document']['file_id'];
                $file = \Yii::$app->telegram->getFile([
                    'file_id' => $fileId
                ]);

                $fileName = time() . '-' .$this->data['message']['document']['file_name'];
                file_put_contents(\Yii::getAlias('@api') . '/web/assets/' . $fileName, $file);

                $this->sendMessage('<i>Файл прикреплен</i>');
                $this->createJiraTask($project->key, $session['title'], $session['detail'], $authorId, $fileName, $session['priority']);
            } elseif (mb_strtolower($message) == 'нет') {
                $this->sendMessage('<i>Файл не прикреплен</i>');
                $this->createJiraTask($project->key, $session['title'], $session['detail'], $authorId,false,  $session['priority']);
            }
        }
    }

    public function addTelegramId($telegram) {
        $tg_username = $telegram->input->message->from->username;
        $tg_user = TgJiraUser::findOne(['telegram_username' => $tg_username]);

        if ($tg_user && $tg_user->user_id) {
            $user = User::findOne(['id' => $tg_user->user_id]);
            $user->updateAttributes(['telegram_id' => $telegram->input->message->chat->id]);
        }
    }

    public function getSession() {
        $session = [];
        if (\Yii::$app->redis->get($this->chatId)) {
            $session = json_decode(\Yii::$app->redis->get($this->chatId), JSON_FORCE_OBJECT);
        }
        return $session;
    }

    public function setSession($step, $key, $data) {
        $session = $this->getSession();
        $session[$key] = $data;
        $session['step'] = $step;

        \Yii::$app->redis->set($this->chatId, json_encode($session, JSON_FORCE_OBJECT));
    }

    public function handleCallback() {
        $this->chatId = $this->data['callback_query']['message']['chat']['id'];
        $data = $this->data['callback_query']['data'];
        if ($data) {
            $this->setSession(1, 'project', $data);
            $this->sendMessage("<b>Введите тему</b>");
        }
    }

    public function clearSession() {
        \Yii::$app->redis->set($this->chatId, null);
    }

    public function handleCreateTask() {
        $projects = JiraProjects::find()->all();
        $keyboard = [];
        $temp = [];

        foreach ($projects as $key => $project) {
            $temp[] = ['text' => $project->name, 'callback_data' => 'project-' . $project->id ];
            if (($key + 1) % 3 == 0) {
                $keyboard[] = $temp;
                $temp = [];
            } elseif ((sizeof($projects) - 2) < $key) {
                $keyboard[] = $temp;
            }
        }


        if (empty($keyboard)) {
           $this->sendMessage('<b>Проектов нет</b>');
        } else {
            \Yii::$app->telegram->sendMessage([
                'chat_id' => $this->chatId,
                'text' => '<b>Выберите проект</b>',
                'parse_mode' => 'html',
                'reply_markup' => json_encode([
                    'inline_keyboard' => $keyboard,
                    'resize_keyboard' => true
                ])
            ]);
        }
    }

    public function sendMessage($msg) {
        \Yii::$app->telegram->sendMessage([
            'chat_id' => $this->chatId,
            'text' => $msg,
            'parse_mode' => 'html',
            'disable_web_page_preview' => true,
            'reply_markup' => json_encode([
                'keyboard' => [
                    [
                        ['text' => '◀️Вернуться в меню']
                    ]
                ],
                'resize_keyboard' => true
            ])
        ]);
    }

    public function createJiraTask($projectKey, $title, $detail, $authorId, $file, $priority) {
        $curl = curl_init();
        $jira_project = JiraProjects::findOne(['key' => $projectKey]);
        if ($jira_project->default_task_type_id == JiraProjects::DEFAULT_ISSUE_TYPE_TASK) {
            $taskType = 'Task';
        } else {
            $taskType = 'Bug';
        }

        $reporter = '';

        if ($authorId) {
            $reporter = '"reporter": {"id": "'.$authorId.'"},';
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://'.JiraService::$jira_domain.'.atlassian.net/rest/api/3/issue',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "fields": {
                   "project":
                   {
                      "key": "'.$projectKey.'"
                   },
                   '.$reporter.'
                   "summary": "'.$title.'",
                   "description": {
                        "type": "doc",
                        "version": 1,
                        "content": [
                            {
                            "type": "paragraph",
                            "content": [
                                {
                                "text": "'.$detail.'",
                                "type": "text"
                                }
                            ]
                            }
                        ]
                    },
                   "issuetype": {
                      "name": "'.$taskType.'"
                   }
               }
            }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . JiraService::$jira_token,
                'Content-Type: application/json',
                'Cookie: atlassian.xsrf.token=7ed0d05f-a7f7-4cb4-b6fc-5a231a73ec20_c7736b874cc29a6a4e1905fc292a22460b5b8ce5_lin'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($response, JSON_FORCE_OBJECT);

        if ($file) {
            $this->attachFileToTask($response['id'], $file);
        }
        $this->setPriority($this->getPriorityByName($priority), $response['key']);

        $this->sendMessage("Задача создана <a href='https://".JiraService::$jira_domain.".atlassian.net/browse/{$response['key']}'><b>{$response['key']}</b></a>");
        $this->clearSession();
        $this->createMenu(\Yii::$app->telegram);
    }

    public function getPriorityByName($priorityName) {
        $priority = 'Medium';
        if ($priorityName == 'Очень Важно') {
            $priority = 'Highest';
        } elseif ($priorityName == 'Важно') {
            $priority = 'High';
        } elseif ($priorityName == 'Средне') {
            $priority = 'Medium';
        } elseif ($priorityName == 'Не Важно') {
            $priority = 'Low';
        }
        return $priority;
    }

    public function setPriority($priority, $taskId) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://'.JiraService::$jira_domain.'.atlassian.net/rest/api/3/issue/' . $taskId,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS =>'{
                "update": {
                    "priority": [{
                        "set": {
                            "name": "'.$priority.'"
                        }
                    }]
                }
            }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . JiraService::$jira_token,
                'Content-Type: application/json',
                'Cookie: atlassian.xsrf.token=7ed0d05f-a7f7-4cb4-b6fc-5a231a73ec20_c7736b874cc29a6a4e1905fc292a22460b5b8ce5_lin'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
    }

    public function attachFileToTask($taskId, $file) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://".JiraService::$jira_domain.".atlassian.net/rest/api/3/issue/{$taskId}/attachments",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('file'=> new \CURLFile(__DIR__ . '/../web/assets/' . $file)),
            CURLOPT_HTTPHEADER => array(
                    'X-Atlassian-Token: no-check',
                    'Authorization: Basic ' . JiraService::$jira_token,
                    'Cookie: atlassian.xsrf.token=7ed0d05f-a7f7-4cb4-b6fc-5a231a73ec20_c7736b874cc29a6a4e1905fc292a22460b5b8ce5_lin'
                ),
            ));

        $response = curl_exec($curl);

        curl_close($curl);
    }
}