<?php

namespace api\services;

use common\models\TgJiraUser;
use yii\bootstrap\NavBar;

class GitService
{
    public $data;
    public $key_words = ['fix', 'fixed', 'resolve', 'resolved', 'done', 'finished', 'closed'];
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function handle() {
        if ($this->data['event_name'] == 'push') {
            $this->handlePushEvent();
        } elseif ($this->data['event_name'] == 'merge_request') {
            $this->handleMergeRequestEvent();
        }
    }

    public function handlePushEvent() {
        $ref = explode('/', $this->data['ref']);
        $branchName = end($ref);

        if ($branchName == 'master' || $branchName == 'develop') {
            $taskName = $this->getTaskName($this->data['commits']);

            if ($taskName) {
                $jira = new JiraService($this->data);
                $statuses = $jira->getAvailableStatuses($taskName);
                $statusId = 11;
                
                foreach ($statuses as $status) {
                    if (mb_strtolower($status['to']['name']) == 'готово' && $branchName == 'master') {
                        $statusId = $status['id'];
                        break;
                    }
                    if (mb_strtolower($status['to']['name']) == 'review' && $branchName == 'develop') {
                        $statusId = $status['id'];
                        break;
                    }
                }
                $jira->setTaskStatus($statusId, $taskName);
            }
        }
    }

    public function handleMergeRequestEvent() {
        // No need for now
    }

    public function getTaskName($commits) {
        if (!is_array($commits)) {
            return false;
        }

        if (strpos('Merge branch', end($commits)['message']) !== false) {
            $commit = $commits[count($commits) - 2];
        } else {
            $commit = end($commits);
        }

        $chunk = explode(' ', preg_replace("/\r|\n/", "", $commit['message']));
        foreach ($chunk as $key => $word) {
            $word = mb_strtolower($word);
            if (in_array($word, $this->key_words)) {
                if (isset($chunk[$key + 1])) {
                    return mb_strtoupper($chunk[$key + 1]);
                }
            }
        }
        return false;
    }
}