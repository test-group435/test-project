<?php


namespace api\services;


use common\models\JiraProjects;
use common\models\TgJiraUser;

class JiraService
{
    public static $jira_domain = 'jgt-test-flow';
    public static $jira_token = 'c2Fuemhhci5zYXJzZW5ieUB5YW5kZXgucnU6am51akFaNXU5QkkzcWdWOENJa085NUZB';

    public $data;

    const STATUS_TO_DO = 11;
    const STATUS_IN_PROGRESS = 21;
    const STATUS_TAKE_TO_WORK = 51;
    const STATUS_DONE = 31;
    const STATUS_REVIEW = 41;

    public static $statuses = [
        self::STATUS_TO_DO => 'К выполнению',
        self::STATUS_IN_PROGRESS => 'В работе',
        self::STATUS_DONE => 'Готово',
        self::STATUS_REVIEW => 'На проверке',
    ];


    public static $jira_changelog_statuses = [
        'TO DO' => 'К выполнению',
        'IN PROGRESS' => 'В работе',
        'DONE' => 'Готово',
        'REVIEW' => 'На проверке',
    ];

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle() {
        if ($this->data['webhookEvent'] == 'jira:issue_updated') {
            $this->issueUpdatedNotify();
        } elseif ($this->data['webhookEvent'] == 'jira:issue_created') {
            $project_key = explode('-', $this->data['issue']['key'])[0];
            $project = JiraProjects::findOne(['key' => $project_key]);

            if ($project->teamLead && $project->teamLead->telegram_id) {
                $chatId = $project->teamLead->telegram_id;
                \Yii::$app->telegram->sendMessage([
                    'chat_id' => $chatId,
                    'text' => "Создана новая задача <a href='https://".JiraService::$jira_domain.".atlassian.net/browse/{$this->data['issue']['key']}'><b>{$this->data['issue']['key']}</b></a>",
                    'parse_mode' => 'html',
                    'disable_web_page_preview' => true
                ]);
            }
        } elseif ($this->data['webhookEvent'] == 'comment_created') {
            $assignee = TgJiraUser::findOne(['jira_username' => $this->data['issue']['fields']['assignee']['displayName']]);
            $comment = strip_tags($this->data['comment']['body']);
            $parts = explode('|', $comment);
            $comment = $parts[0];
            $telegram = '';
            // test
            if (isset($parts[1])) {
                $telegram = $parts[1];
                if (strpos($telegram, '@') !== false) {
                    $length = strpos($telegram, '@') - strpos($telegram, '|');
                    $telegram = substr($telegram, strpos($telegram, '@'), $length);
                }
            }

            $comment = str_replace('_', '', $comment);
            $comment = str_replace('*', '', $comment);

            \Yii::$app->telegram->sendMessage([
                'chat_id' => $assignee->user->telegram_id,
                'text' => "В задаче <a href='https://".JiraService::$jira_domain.".atlassian.net/browse/{$this->data['issue']['key']}'><b>{$this->data['issue']['key']}</b></a> новый комментарий | <i>{$comment} {$telegram}</i>",
                'parse_mode' => 'html',
                'disable_web_page_preview' => true
            ]);
        } elseif ($this->data['webhookEvent'] == 'comment_updated') {
            // Это нужно или нет не помню )
        }
    }

    public function issueUpdatedNotify() {
        $key = $this->data['issue']['key'];

        foreach ($this->data['changelog']['items'] as $item) {
            $notification = '';

            if ($item['field'] == 'status') {
                if (isset($data['issue']['fields']['reporter'])) {
                    $reporter = $this->data['issue']['fields']['reporter']['displayName'];
                } else {
                    $reporter = $this->data['issue']['fields']['creator']['displayName'];
                }

                $from = JiraService::$jira_changelog_statuses[mb_strtoupper($item['fromString'])];
                $to = JiraService::$jira_changelog_statuses[mb_strtoupper($item['toString'])];

                $creator = TgJiraUser::findOne(['jira_username' => $reporter]);
                $notification .=
                    "Статус задачи <a href='https://".JiraService::$jira_domain.".atlassian.net/browse/{$key}'><b>{$key}</b></a> был изменен со статуса <i>'{$from}'</i> на статус <i>'{$to}'</i>";

                if ($creator && $creator->user && $creator->user->telegram_id) {
                    \Yii::$app->telegram->sendMessage([
                        'chat_id' => $creator->user->telegram_id,
                        'text' => $notification,
                        'parse_mode' => 'html',
                        'disable_web_page_preview' => true
                    ]);
                }
            } elseif ($item['field'] == 'assignee') {
                $assigneeName = $item['toString'];

                $assignee = TgJiraUser::findOne(['jira_username' => $assigneeName]);
                $notification .= "На вас была назначена задача <a href='https://".JiraService::$jira_domain.".atlassian.net/browse/{$key}'><b>{$key}</b></a> | <i>{$this->data['issue']['fields']['summary']}</i>";

                if ($assignee && $assignee->user && $assignee->user->telegram_id) {
                    \Yii::$app->telegram->sendMessage([
                        'chat_id' => $assignee->user->telegram_id,
                        'text' => $notification,
                        'parse_mode' => 'html',
                        'disable_web_page_preview' => true
                    ]);
                }
            }
        }
    }

    public function getAvailableStatuses($taskName) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://'.JiraService::$jira_domain.'.atlassian.net/rest/api/3/issue/'.$taskName.'/transitions',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . JiraService::$jira_token,
                'Cookie: atlassian.xsrf.token=b23cfa38-d552-4b6e-9261-e8225bdf60c2_964a06afbc3a98187d0ed119611ebca7f2173292_lin'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, JSON_FORCE_OBJECT)['transitions'];
    }

    public function setTaskStatus($statusId, $taskName) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://".self::$jira_domain.".atlassian.net/rest/api/3/issue/{$taskName}/transitions",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "transition": {
                    "id": "'.$statusId.'"
                }
            }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . self::$jira_token,
                'Content-Type: application/json',
                'Cookie: atlassian.xsrf.token=7ed0d05f-a7f7-4cb4-b6fc-5a231a73ec20_8e5109600f604e25f9d43710f22245ab629d9609_lin'
            ),
        ));
        $response = curl_exec($curl);

        curl_close($curl);
    }

    public function setAssignee($jira_id, $key) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://'.self::$jira_domain.'.atlassian.net/rest/api/3/issue/'.$key.'/assignee',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS =>'{
                  "accountId": "'.$jira_id.'"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Basic ' . self::$jira_token,
                    'Content-Type: application/json',
                    'Cookie: atlassian.xsrf.token=b23cfa38-d552-4b6e-9261-e8225bdf60c2_124ec56d670fbc71a1055f81b2303bd10872bcf2_lin'
                ),
            ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}