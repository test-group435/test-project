<?php


namespace admin\models;


class User extends \common\models\User
{
    public $pass;
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->setPassword($this->pass);
            $this->generateAuthKey();
            $this->status = 10;
        }
        return parent::beforeSave($insert);
    }


}