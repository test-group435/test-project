<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TgJiraUser;

/**
 * TgJiraUserSearch represents the model behind the search form of `common\models\TgJiraUser`.
 */
class TgJiraUserSearch extends TgJiraUser
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['telegram_username', 'jira_username', 'gitlab_username', 'jira_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TgJiraUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'telegram_username', $this->telegram_username])
            ->andFilterWhere(['like', 'gitlab_username', $this->gitlab_username])
            ->andFilterWhere(['like', 'jira_id', $this->jira_id])
            ->andFilterWhere(['like', 'jira_username', $this->jira_username]);

        return $dataProvider;
    }
}
