<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TgJiraUser */

$this->title = 'Create Tg Jira User';
$this->params['breadcrumbs'][] = ['label' => 'Tg Jira Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tg-jira-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
