<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TgJiraUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tg-jira-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'telegram_username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jira_username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gitlab_username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jira_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\select2\Select2::class, [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->all(), 'id','username'),
        'pluginOptions' => [
                'allowClear' => true
        ],
        'options' => ['prompt' => '']
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
