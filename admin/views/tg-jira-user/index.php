<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\TgJiraUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tg Jira Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tg-jira-user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tg Jira User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'telegram_username',
            'jira_username',
            'gitlab_username',
            'jira_id',
            [
                'attribute' => 'user_id',
                'filter' => \kartik\select2\Select2::widget([
                    'attribute' => 'user_id',
                    'model' => $searchModel,
                    'data' => \yii\helpers\ArrayHelper::map(\admin\models\User::find()->all(), 'id', 'username'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'options' => ['prompt' => '']
                ]),
                'value' => function($model) {
                    return $model->user->username;
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
