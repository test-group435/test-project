<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JiraProjects */

$this->title = 'Create Jira Projects';
$this->params['breadcrumbs'][] = ['label' => 'Jira Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jira-projects-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
