<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\JiraProjects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jira-projects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'default_task_type_id')->dropDownList(\common\models\JiraProjects::$default_task_types) ?>

    <?= $form->field($model, 'default_spawn_place_id')->dropDownList(\common\models\JiraProjects::$default_spawn_places) ?>

    <?= $form->field($model, 'team_lead_id')->widget(\kartik\select2\Select2::class, [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'),
        'pluginOptions' => ['allowClear' => true],
        'options' => ['prompt'=>'']
    ])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
