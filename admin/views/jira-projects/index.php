<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\JiraProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jira Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jira-projects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Jira Projects', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'key',
            [
                'attribute' => 'team_lead_id',
                'filter' => \kartik\select2\Select2::widget([
                    'attribute' => 'team_lead_id',
                    'model' => $searchModel,
                    'data' => \yii\helpers\ArrayHelper::map(\admin\models\User::find()->all(), 'id', 'username'),
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'options' => ['prompt' => '']
                ]),
                'value' => function($model) {
                    return $model->teamLead->username;
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
