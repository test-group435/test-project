<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserProjects */

$this->title = 'Create User Projects';
$this->params['breadcrumbs'][] = ['label' => 'User Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-projects-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
