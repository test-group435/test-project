<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\UserProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-projects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User Projects', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'filter' => \kartik\select2\Select2::widget([
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'),
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'pluginOptions' => ['allowClear' => true],
                    'options' => ['prompt' => '']
                ]),
                'value' => function($model) {
                    return $model->user->username;
                }
            ],
            [
                'attribute' => 'project_id',
                'filter' => \kartik\select2\Select2::widget([
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\JiraProjects::find()->all(), 'id', 'name'),
                    'model' => $searchModel,
                    'attribute' => 'project_id',
                    'pluginOptions' => ['allowClear' => true],
                    'options' => ['prompt' => '']
                ]),
                'value' => function($model) {
                    return $model->project->name;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
