<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
Yii::$app->name = 'Админ панель сети'
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">Сеть</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <a href="<?= \yii\helpers\Url::to(['site/logout']) ?>" class="btn btn-warning" style="margin: 7px">Выйти</a>
        </div>
    </nav>
</header>
