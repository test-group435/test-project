<?php

use yii\db\Migration;

/**
 * Class m201224_130641_add_col_jira_id
 */
class m201224_130641_add_col_jira_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tg_jira_users', 'jira_id', $this->string());
        $this->dropColumn('tg_jira_users', 'jira_token');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tg_jira_users', 'jira_id');
        $this->addColumn('tg_jira_users', 'jira_token', $this->string());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201224_130641_add_col_jira_id cannot be reverted.\n";

        return false;
    }
    */
}
