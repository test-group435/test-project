<?php

use yii\db\Migration;

/**
 * Class m201223_055529_add_column_key_to_jira_projects
 */
class m201223_055529_add_column_key_to_jira_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('jira_projects', 'key', $this->string()->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jira_projects', 'key');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_055529_add_column_key_to_jira_projects cannot be reverted.\n";

        return false;
    }
    */
}
