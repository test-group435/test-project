<?php

use yii\db\Migration;

/**
 * Class m201229_062416_create_table_user_projects_relation
 */
class m201229_062416_create_table_user_projects_relation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_projects', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'project_id' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-user_projects-user_id',
            'user_projects',
            'user_id',
            'user',
            'id',
            'CASCADE'
            );

        $this->addForeignKey(
            'fk-user_projects-project_id',
            'user_projects',
            'project_id',
            'jira_projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user_projects-user_id', 'user_projects');
        $this->dropForeignKey('fk-user_projects-project_id', 'user_projects');
        $this->dropTable('user_projects');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201229_062416_create_table_user_projects_relation cannot be reverted.\n";

        return false;
    }
    */
}
