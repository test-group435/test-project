<?php

use yii\db\Migration;

/**
 * Class m201224_111843_add_git_col_to_jira
 */
class m201224_111843_add_git_col_to_jira extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tg_jira_users', 'gitlab_username', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tg_jira_users', 'gitlab_username');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201224_111843_add_git_col_to_jira cannot be reverted.\n";

        return false;
    }
    */
}
