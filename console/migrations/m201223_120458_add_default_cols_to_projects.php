<?php

use yii\db\Migration;

/**
 * Class m201223_120458_add_default_cols_to_projects
 */
class m201223_120458_add_default_cols_to_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('jira_projects', 'default_task_type_id', $this->integer()->defaultValue(1));
        $this->addColumn('jira_projects', 'default_spawn_place_id', $this->integer()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jira_projects', 'default_task_type_id');
        $this->dropColumn('jira_projects', 'default_spawn_place_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_120458_add_default_cols_to_projects cannot be reverted.\n";

        return false;
    }
    */
}
