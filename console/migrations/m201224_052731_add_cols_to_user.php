<?php

use yii\db\Migration;

/**
 * Class m201224_052731_add_cols_to_user
 */
class m201224_052731_add_cols_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'telegram_id', $this->string());
        $this->addColumn('tg_jira_users', 'user_id', $this->integer());
        $this->addColumn('jira_projects', 'team_lead_id', $this->integer());
        $this->addForeignKey(
            'fk-user_id_in_tg_jira_users',
            'tg_jira_users',
            'user_id',
            'user',
            'id'
        );
        $this->addForeignKey(
            'fk-user_id_in_jira_projects_users',
            'jira_projects',
            'team_lead_id',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'telegram_id');
        $this->dropForeignKey('fk-user_id_in_tg_jira_users', 'tg_jira_users');
        $this->dropForeignKey('fk-user_id_in_jira_projects_users', 'jira_projects');
        $this->dropColumn('jira_projects', 'team_lead_id');
        $this->dropColumn('tg_jira_users', 'user_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201224_052731_add_cols_to_user cannot be reverted.\n";

        return false;
    }
    */
}
