<?php

use yii\db\Migration;

/**
 * Class m201224_121453_add_tokens
 */
class m201224_121453_add_tokens extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tg_jira_users', 'jira_token', $this->string());
        $this->addColumn('jira_projects', 'default_token', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jira_projects', 'default_token');
        $this->dropColumn('tg_jira_users', 'jira_token');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201224_121453_add_tokens cannot be reverted.\n";

        return false;
    }
    */
}
