<?php

use yii\db\Migration;

/**
 * Class m201223_104919_create_table_telegram_jira_usernames
 */
class m201223_104919_create_table_telegram_jira_usernames extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tg_jira_users', [
            'id' => $this->primaryKey(),
            'telegram_username' => $this->string()->unique(),
            'jira_username' => $this->string()->unique()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tg_jira_users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_104919_create_table_telegram_jira_usernames cannot be reverted.\n";

        return false;
    }
    */
}
