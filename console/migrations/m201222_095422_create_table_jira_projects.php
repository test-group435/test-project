<?php

use yii\db\Migration;

/**
 * Class m201222_095422_create_table_jira_projects
 */
class m201222_095422_create_table_jira_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jira_projects', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('jira_projects');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201222_095422_create_table_jira_projects cannot be reverted.\n";

        return false;
    }
    */
}
