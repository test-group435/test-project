<?php
namespace console\controllers;

use common\models\User;
use yii\console\Controller;

class TestController extends Controller
{
    public $telegramUrl = 'https://api.telegram.org/bot';
    public function actionCreateUser() {
        $user = new User();
        $user->username = 'admin';
        $user->setPassword('123');
        $user->generateAuthKey();
        $user->status = 10;
        $user->save();
    }

    public function actionSetTelegramWebhook($webhookUrl) {
        $url = $this->telegramUrl . \Yii::$app->telegram->botToken . "/setWebhook?url=" . $webhookUrl;
        $response = file_get_contents($url);
        var_dump($response);
    }
}